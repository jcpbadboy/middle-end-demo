<#include 'common/header.ftl' />
<script src="/dist/js/order.js?version=4"></script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            支付中心
        </h1>

    </section>

    <!-- Main context -->
    <section class="content">

        <div class="box">
            <button onclick="pay('${order_sn}','alipayDirectPlugin')">支付宝</button>
            <button onclick="pay('${order_sn}','weixinPayPlugin')">微信</button>
            <div class="pay-qrcode" id="pay-qrcode">
                <iframe id="iframe-qrcode" width="200px" height="200px" scrolling="no"></iframe>
            </div>
        </div>

    </section>
    <!-- /.context -->
</div>



<!-- 新部署Modal -->
<#include 'order_modal.ftl' />
<!-- Modal -->



<#include 'common/footer.ftl' />