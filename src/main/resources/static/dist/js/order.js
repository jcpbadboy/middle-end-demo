var table;

$(function () {
    saveDeploy();
    initGrid();
});



function saveDeploy() {

    /**
     * 发出添加请求
     */
    $("#save-btn").click(function () {
        // if( $("#deploy-form").valid() ){
        $("#deploy-form").ajaxSubmit({
            url:"../../data/orders",
            type:"POST",
            success:function () {
                alert("订单创建成功")
                $('#deployModal').modal("hide");
                table.ajax.url("../../data/orders").load();
            }
        }) ;
        // }

    });
}


/**
 * 打开新增部署对话框
 */
function addDeploy() {
    $('#deployModal').modal();
}



/**
 * redis表单控件切换公用方法
 */
function changeRedisForm() {
    var redis_type = $("[name=redis_type]").val();
    var config_type = $("[name=config_type]").val();
    $('[for]').hide();
    if( config_type == 'manual' ){
        $("[for="+ redis_type +"]").removeClass("hide").show();
    }else {
        $("[for="+ config_type +"]").removeClass("hide").show();
    }

}



/**
 * 公用的填充form数据方法
 * @param formid
 * @param data
 */
function fillForm(formid,data) {

    if (!data) {
        return;
    }

    for (value in data) {
        $("#" + formid + " [name='" + value + "']").val(data[value]);
    }

}

function goPay(orderSn){

    location.href="/view/order/pay/"+orderSn;
}

function pay(orderSn,paymentPluginId){
    $.ajax({
        url: "/data/orders/pay?payment_plugin_id="+paymentPluginId+"&order_sn="+orderSn,
        type: "GET",
        success: function (res) {

            if (paymentPluginId === 'weixinPayPlugin') {
                let weixinPayQrImage = `<img src="`+res.gateway_url+`" alt="weixinPayQrImage" />`
                document.getElementById('pay-qrcode').innerHTML = weixinPayQrImage
            }else{
                let $formItems = ''
                res.form_items && res.form_items.forEach(item => {
                    $formItems += `<input name="${item.item_name}" type="hidden" value='${item.item_value}' />`
                })
                let $form = `<form action="`+res.gateway_url+`" method="post">${$formItems}</form>`
                let iframe = '<iframe id="iframe-qrcode" width="200px" height="200px" scrolling="no"></iframe>'

                document.getElementById('pay-qrcode').innerHTML = iframe
                const qrIframe = document.getElementById('iframe-qrcode').contentWindow.document
                qrIframe.getElementsByTagName('body')[0].innerHTML = $form
                qrIframe.forms[0].submit()
            }
            checkPayStatus(orderSn)
        },
        error: function () {
            alert("error")
        }
    })

}

/** 检查微信支付状态 **/
function checkPayStatus(sn) {

    const weixinPayTimer = setInterval(async () => {
        $.ajax({
            url:"/data/orders/check?order_sn="+sn,
            type:"GET",
            success:function (res) {
                if(res == true){
                    clearTimeout(weixinPayTimer)
                    location.href = "/view/order/list"
                }

            }
        })

    }, 2000)




    // this.$watch('paymentList', (list) => {
    //     list.forEach(item => item.plugin_id === 'alipayDirectPlugin' && clearTimeout(weixinPayTimer))
    // })
    // this.$on('hook:beforeDestroy', () => clearTimeout(weixinPayTimer))
}


/**
 * 初始化grid列表
 */
function initGrid() {

    table= $("#deploy_grid").DataTable({
        "paging": false,
        "searching": false,
        ajax: {
            url: '../../data/orders',
        },

        columns: [ //定义列

            {
                data: "order_sn"
            },
            {
                data: "goods_name"
            },
            {
                data: "price"
            },
            {
                data: "remark"
            },
            {
                data: "create_time_text"
            },
            {
                data: "order_status_text"
            },
            {
                data: null,
                "render": function (data, type, row) {
                    var btnHtml = "";
                    btnHtml+='<div class="btn-group deploy_btn_box" >'
                    if(data.order_status_text=="已确认"){
                        btnHtml+='    <button type="button" class="btn btn-info" onclick="goPay(\''+data["order_sn"]+'\')">支付</button>'
                    }
                    btnHtml+='</div>';
                    return btnHtml;
                }
            }
        ]

    });

}

function changeGoods(){

    let goodsName = $("#sku_id").find("option:selected").text();

    $("#goods_name_id").val(goodsName);

}