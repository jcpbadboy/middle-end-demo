package com.enation.app.javashop.middle;

import com.enation.app.javashop.framework.context.instance.InstanceContextImpl;
import com.enation.app.javashop.framework.database.DataSourceConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;


/**
 *
 * 中台调用示例
 * @author kingapex
 * @version v1.0.0
 * @since v7.0.0
 * 2018年1月22日 上午10:01:32
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients("com.enation.app.javashop.core.client")
@ComponentScan(value = "com.enation.app.javashop.framework.database," +
        "com.enation.app.javashop.framework.context," +
        "com.enation.app.javashop.framework.exception," +
        "com.enation.app.javashop.framework.redis.configure.builders,"+
        "com.enation.app.javashop.middle",excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = {InstanceContextImpl.class, DataSourceConfig.class}))

public class MiddleEndDemoApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(MiddleEndDemoApplication.class, args);

    }
}
