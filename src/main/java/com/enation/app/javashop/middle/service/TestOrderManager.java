package com.enation.app.javashop.middle.service;

import com.enation.app.javashop.middle.model.Order;
import com.enation.app.javashop.framework.database.Page;
import com.enation.app.javashop.model.trade.order.dto.OrderDTO;

/**
 * 订单
 * Created by fk on 2020.5.26
 * @author fk
 * @version 1.0
 * @since 7.2.0
 */

public interface TestOrderManager {


    /**
     * 查看订单列表
     * @param page
     * @param pageSize
     * @return
     */
    Page list(int page, int pageSize);

    /**
     * 添加订单
     * @param price
     * @param remark
     * @param skuId
     * @param goodsName
     * @return
     */
    OrderDTO add(Double price, String remark,Integer skuId,String goodsName);

    /**
     * 获取订单
     * @param orderSn
     * @return
     */
    Order get(String orderSn);

}
