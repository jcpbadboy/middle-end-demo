package com.enation.app.javashop.middle.api;

import com.enation.app.javashop.client.payment.PaymentClient;
import com.enation.app.javashop.middle.model.Order;
import com.enation.app.javashop.middle.service.TestOrderManager;
import com.enation.app.javashop.framework.database.Page;
import com.enation.app.javashop.model.payment.dto.PayParam;
import com.enation.app.javashop.model.trade.order.dto.OrderDTO;
import com.enation.app.javashop.model.trade.order.enums.TradeTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Map;

/**
 * 订单创建数据
 * Created by fk on 2020.5.26
 * @author fk
 * @version 1.0
 * @since 7.2.0
 */
@RestController
@RequestMapping("/data/orders")
public class OrderDataController {

    @Autowired
    private TestOrderManager testOrderManager;

    @Autowired
    private PaymentClient paymentClient;

    @GetMapping
    public Page list(@ApiIgnore  Integer pageNo, @ApiIgnore Integer pageSize)	{

        return	this.testOrderManager.list(pageNo,pageSize);
    }


    @PostMapping
    public OrderDTO add(Double price, String remark,Integer skuId,String goodsName){


        return testOrderManager.add(price,remark,skuId,goodsName);
    }

    @GetMapping("/pay")
    public Map pay(String paymentPluginId, String orderSn){


        Order order = testOrderManager.get(orderSn);

        PayParam payParam = new PayParam();
        payParam.setClientType("PC");
        payParam.setPaymentPluginId(paymentPluginId);
        payParam.setPayMode("qr");
        payParam.setSn(orderSn);
        payParam.setTradeType(TradeTypeEnum.ORDER.name());
        payParam.setPrice(order.getPrice());

        return	this.paymentClient.pay(payParam);
    }

    @GetMapping("/check")
    public boolean check(String orderSn){


        String status = paymentClient.queryBill(orderSn, "ORDER");

        if("success".equals(status)){

            return true;
        }
        return false;
    }



}
