package com.enation.app.javashop.middle.model;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.Table;
import com.enation.app.javashop.framework.util.DateUtil;
import com.enation.app.javashop.model.trade.order.enums.OrderStatusEnum;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;


/**
 * 订单实体
 * Created by fk on 2020.5.26
 * @author fk
 * @version 1.0
 * @since 7.2.0
 */
@Table(name = "es_order")
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class Order implements Serializable {

    private static final long serialVersionUID = 7962497529274547L;

    /**
     * 主键
     */
    @Id(name = "id")
    @ApiModelProperty(hidden = true)
    private Integer id;
    /**
     * 订单号
     */
    @Column(name = "order_sn")
    @ApiModelProperty(name = "order_sn", value = "订单号", required = true)
    private String orderSn;

    /**
     * 添加时间
     */
    @Column(name = "create_time")
    private Long createTime;

    @Column(name = "remark")
    private String remark;

    @Column(name = "goods_name")
    private String goodsName;

    @Column(name = "price")
    private Double price;

    @Column(name = "order_status")
    private String orderStatus;

    private String createTimeText;

    private String orderStatusText;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getCreateTimeText() {
        if (createTime != null) {
            createTimeText = DateUtil.toString(createTime, "yyyy-MM-dd HH:MM:ss");
            return createTimeText;
        }
        return "";
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getOrderStatusText() {

        if (orderStatus != null) {
            this.orderStatusText = OrderStatusEnum.valueOf(orderStatus).description();
            return orderStatusText;
        }

        return "";
    }

    public void setOrderStatusText(String orderStatusText) {

        this.orderStatusText = orderStatusText;
    }

    public void setCreateTimeText(String createTimeText) {
        this.createTimeText = createTimeText;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }
}