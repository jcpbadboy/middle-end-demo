package com.enation.app.javashop.middle.config;

import com.enation.app.javashop.framework.context.instance.AppInstance;
import com.enation.app.javashop.framework.context.instance.InstanceContext;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

/**
 * 适配实例注册，在本例中没有实际作用，主要为了适配使启动不报错
 * @author kingapex
 * @version 1.0
 * @since 7.1.0
 * 2020/6/29
 */
@Service
@Primary
public class DemoInstanceRegister implements InstanceContext {
    @Override
    public Set<String> getApps() {
        return null;
    }

    @Override
    public List<AppInstance> getInstances() {
        return null;
    }

    @Override
    public void register() {

    }
}
