package com.enation.app.javashop.middle.consumer;


import com.enation.app.javashop.framework.database.DaoSupport;
import com.enation.app.javashop.framework.logs.Logger;
import com.enation.app.javashop.framework.logs.LoggerFactory;
import com.enation.app.javashop.model.base.message.OrderStatusChangeMsg;
import com.enation.app.javashop.model.base.rabbitmq.AmqpExchange;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 订单状态改变消费者
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0 2018年3月23日 上午10:31:42
 */
@Component
public class OrderStatusChangeConsumer {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private DaoSupport daoSupport;

    /**
     * 绑定一个队列到订单状态变化的交换机
     * 用来接收订单状态变化消息
     *
     * @param orderMessage
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.ORDER_STATUS_CHANGE + "_DEMO_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.ORDER_STATUS_CHANGE, type = ExchangeTypes.FANOUT)
    ))
    public void orderChange(OrderStatusChangeMsg orderMessage)  throws InterruptedException {
        //订单编号
        String orderSn = orderMessage.getOrderDO().getSn();

        String sql = "update es_order set order_status = ? where order_sn = ?";

        this.daoSupport.execute(sql,orderMessage.getNewStatus().value(),orderSn);
    }

}
