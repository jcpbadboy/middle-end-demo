package com.enation.app.javashop.middle.service.impl;


import com.enation.app.javashop.client.trade.OrderCenterClient;
import com.enation.app.javashop.middle.model.Order;
import com.enation.app.javashop.middle.service.TestOrderManager;
import com.enation.app.javashop.framework.database.DaoSupport;
import com.enation.app.javashop.framework.database.Page;
import com.enation.app.javashop.framework.util.DateUtil;
import com.enation.app.javashop.model.trade.cart.vo.PriceDetailVO;
import com.enation.app.javashop.model.trade.order.dto.OrderDTO;
import com.enation.app.javashop.model.trade.order.enums.PaymentTypeEnum;
import com.enation.app.javashop.model.trade.order.vo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 订单
 * Created by fk on 2020.5.26
 * @author fk
 * @version 1.0
 * @since 7.2.0
 */
@Service
public class TestOrderManagerImpl implements TestOrderManager {

    @Autowired
    private DaoSupport daoSupport;

    @Autowired
    private OrderCenterClient orderCenterClient;

    @Override
    public Page list(int page, int pageSize) {
        String sql = "select * from es_order";
        return daoSupport.queryForPage(sql, page, pageSize, Order.class);
    }

    @Override
    public OrderDTO add(Double price, String remark,Integer skuId,String goodsName) {

        //设置地区
        ConsigneeVO consignee = new ConsigneeVO();
        consignee.setProvince("北京");
        consignee.setCity("朝阳区");
        consignee.setCounty("三环到四环之间");
        consignee.setProvinceId(1);
        consignee.setCityId(72);
        consignee.setCountyId(2819);
        consignee.setAddress("花园路1号院");
        consignee.setName("张三");
        consignee.setMobile("13688888888");

        PriceDetailVO priceDetailVO = new PriceDetailVO();
        priceDetailVO.setGoodsPrice(price);
        priceDetailVO.setTotalPrice(price);

        //创建交易对象
        TradeParam tradeParam = new TradeParam();
        tradeParam.setConsignee(consignee);
        tradeParam.setClient("PC");
        tradeParam.setPaymentType(PaymentTypeEnum.ONLINE.value());
        tradeParam.setPrice(priceDetailVO);
        tradeParam.setReceiveTime("任意时间");
        tradeParam.setRemark(remark);
        tradeParam.setMemberName("会员名称");
        tradeParam.setMemberId(-1);
        tradeParam.setShipName("张三");
        tradeParam.setMobile("13251415414");
        TradeVO tradeVO = orderCenterClient.createTradeVO(tradeParam);

        OrderParam param = new OrderParam();
        param.setRemark(remark);
        param.setPrice(priceDetailVO);
        param.setMemberName("会员名称");
        param.setMemberId(-1);
        param.setSellerId(-1);
        param.setSellerName("新对接系统");
        param.setReceiveTime("任意时间");
        param.setGoodsNum(1);
        param.setOrderType("NEW");
        param.setClientType("PC");
        param.setIsSiteCoupon(false);
        param.setPaymentType(PaymentTypeEnum.ONLINE.value());

        param.setConsignee(consignee);
        //商品信息
        List<OrderSkuVO> skuList = new ArrayList<>();
        OrderSkuVO orderSkuVO = new OrderSkuVO();
        orderSkuVO.setName(goodsName);
        orderSkuVO.setSkuId(skuId);
        orderSkuVO.setNum(1);
        orderSkuVO.setGoodsImage("http://javashop-statics.oss-cn-beijing.aliyuncs.com/demo/E1EE42BA506F4A2895B449F661D1644B.jpg_300x300");
        orderSkuVO.setSellerId(15);
        orderSkuVO.setSellerName("食品酒水");
        orderSkuVO.setGoodsId(314);
        orderSkuVO.setCatId(7);
        orderSkuVO.setSkuSn("0077");
        orderSkuVO.setOriginalPrice(0.01);
        orderSkuVO.setPurchasePrice(0.01);
        orderSkuVO.setActualPayTotal(0.01);
        skuList.add(orderSkuVO);

        param.setSkuParam(skuList);
        //创建订单对象
        OrderDTO order = orderCenterClient.createOrderDTO(param);
        List<OrderDTO> list = new ArrayList<>();
        list.add(order);
        tradeVO.setOrderList(list);

        orderCenterClient.createTrade(tradeVO);

        Order o = new Order();
        o.setOrderSn(order.getSn());
        o.setRemark(order.getRemark());
        o.setCreateTime(DateUtil.getDateline());
        o.setPrice(order.getOrderPrice());
        o.setOrderStatus(order.getOrderStatus());
        o.setGoodsName(goodsName);
        this.daoSupport.insert(o);

        return order;
    }

    @Override
    public Order get(String orderSn) {
        String sql = "select * from es_order where order_sn = ?";
        return this.daoSupport.queryForObject(sql, Order.class, orderSn);
    }


}
