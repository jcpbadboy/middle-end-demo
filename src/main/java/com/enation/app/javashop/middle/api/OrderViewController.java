package com.enation.app.javashop.middle.api;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 订单跳转
 * Created by fk on 2020.5.26
 * @author fk
 * @version 1.0
 * @since 7.2.0
 */
@Controller
@RequestMapping("/view/order")
public class OrderViewController {

    @RequestMapping("/list")
    public String index(){

        return "order_list";
    }

    @RequestMapping("/pay/{order_sn}")
    public String pay(@PathVariable("order_sn")String orderSn, Model model){

        model.addAttribute("order_sn",orderSn);
        return "pay";
    }
}
