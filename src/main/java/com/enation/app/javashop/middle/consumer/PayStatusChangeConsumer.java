package com.enation.app.javashop.middle.consumer;


import com.enation.app.javashop.framework.logs.Logger;
import com.enation.app.javashop.framework.logs.LoggerFactory;
import com.enation.app.javashop.model.base.message.PaymentBillStatusChangeMsg;
import com.enation.app.javashop.model.base.rabbitmq.AmqpExchange;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 订单状态改变消费者
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0 2018年3月23日 上午10:31:42
 */
@Component
public class PayStatusChangeConsumer {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    protected final static int MAX_TIMES_CHANGE = 3;

    /**
     * 绑定一个队列到支付账单状态变化的交换机
     * 用来接收支付账单状态变化消息
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.PAYMENT_BILL_CHANGE + "_DEMO_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.PAYMENT_BILL_CHANGE, type = ExchangeTypes.FANOUT)
    ))
    public void billChange(PaymentBillStatusChangeMsg paymentBillMessage){


    }


}
