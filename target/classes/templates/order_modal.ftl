<div class="modal fade" id="deployModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">创建订单</h4>
            </div>
            <div class="modal-body">

                <div class="box-body">

                    <form  id="deploy-form">
                        <div class="form-group">
                            <input type="hidden" name="goods_name"  id="goods_name_id" value="促销商品C">
                            <label>商品</label>
                            <select class="form-control" name="sku_id" id="sku_id" onchange="changeGoods()">
                                <option value="517">促销商品C</option>
                                <option value="530">爱仕达（ASD) 炒锅 NWG8330E 30CM易清洗</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>订单价格</label>
                            <input type="text" name="price" class="form-control" placeholder="请输入订单价格">
                        </div>

                        <div class="form-group">
                            <label>备注</label>
                            <input type="text" name="remark"  class="form-control" placeholder="请输入备注">
                        </div>
                    </form>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary" id="save-btn">确定</button>
            </div>
        </div>
    </div>
</div>
