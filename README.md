# middle-end-demo

#### 介绍

![image-20200630133707658](README.assets/image-20200630133707658.png)

1. 本demo的目的是为了快速体验Javashop电商中台的架构思路，以及基于中台搭建子业务搭建的思路。
2. 中台的订单中心外提供了下单接口，支付中心提供了调起支付接口，查询账单等接口。
3. 在订单状态发生变化时，会有相应的消息发送的消息总线（rabbitmq的交换机上），子业务通过绑定到总线的队列来接收订单变化消息

![image-20200630162326166](README.assets/image-20200630162326166.png)



#### 体验步骤

1. 下载源码

   git clone https://gitee.com/javashop/middle-end-demo.git

2. 运行MiddleEndDemoApplication

3. 浏览器访问http://localhost:8013/view/order/list

4. 通过下单和支付操作体验调用过程




#### 代码说明

为了方便启动演示，本示例采用的嵌入式数据库H2

1、pom引用

```xml
<dependency>
    <groupId>com.enation.app.javashop</groupId>
    <artifactId>spring-cloud-impl</artifactId>
    <version>7.2.1</version>
</dependency>
```

子业务要调用中台的服务，需要引入spring-cloud-impl依赖，这里包含订单、商品、会员等等的核心领域模型以及相应client。

2、订单子业务

这里模拟了一个子业务，有自己的订单表（es_order）,用来存储子业务个性化的数据，如票务系统的座位号、家政服务的上门时间等。

```java

/**
订单中心 client
**/
@Autowired
private OrderCenterClient orderCenterClient;

@Override
public OrderDTO add(...){
    //创建交易对象
    TradeParam tradeParam = new TradeParam();
    tradeParam.set...
    //根据参数创建交易对象信息
    TradeVO tradeVO = orderCenterClient.createTradeVO(tradeParam);

    //设置订单参数
    OrderParam param = new OrderParam();
    param.set...
      
    //商品信息
    List<OrderSkuVO> skuList = new ArrayList<>();
    OrderSkuVO orderSkuVO = new OrderSkuVO();
    orderSkuVO.set...
    skuList.add(orderSkuVO);
    param.setSkuParam(skuList);
  
    //创建订单对象，order对象中会有存有订单编号等信息
    OrderDTO order = orderCenterClient.createOrderDTO(param);
    List<OrderDTO> list = new ArrayList<>();
    list.add(order);
    tradeVO.setOrderList(list);
  
    //最后调用接口创建交易
    orderCenterClient.createTrade(tradeVO);
  
   //子业务个性化功能
    Order o = new Order();
    o.setOrderSn(order.getSn());
    //设置座位号
    o.setSeatNo(order.getRemark());
  
    this.daoSupport.insert(o);
  
}

```

3、支付

调起中台支付功能

```java
/**
支付中心 client
**/
@Autowired
private PaymentClient paymentClient;

/**
调起支付示例
支付中心通过传入的参数创建支付账单，并调用第三方应用创建相应的第三方支付订单。
返回的map配合javascript、vue等技术，拉起支付。
**/
@Override
public Map pay(String paymentPluginId, String orderSn){
  
    PayParam payParam = new PayParam();
    payParam.setClientType("PC");
    payParam.setPaymentPluginId(paymentPluginId);
    payParam.setPayMode("qr");
    payParam.setSn(orderSn);
    payParam.setTradeType(TradeTypeEnum.ORDER.name());
    payParam.setPrice(...);

    return this.paymentClient.pay(payParam);
}
```

4、查询账单状态

```java
@Autowired
private PaymentClient paymentClient;

@Override
public boolean check(String orderSn){
    String status = paymentClient.queryBill(orderSn, "ORDER");

    if("success".equals(status)){

        return true;
    }
    return false;
}
```

5、接收中台mq消息

~~~Java
    /**
     * 绑定一个队列到订单状态变化的交换机
     * 用来接收订单状态变化消息
     *
     * @param orderMessage
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.ORDER_STATUS_CHANGE + "_DEMO_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.ORDER_STATUS_CHANGE, type = ExchangeTypes.FANOUT)
    ))
    public void orderChange(OrderStatusChangeMsg orderMessage)  throws InterruptedException {
      //orderMessage.getOldStatus()  //订单原状态
      //orderMessage.getNewStatus()  //订单新状态
      //orderMessage.getOrderDO()//订单DO
    }
~~~













